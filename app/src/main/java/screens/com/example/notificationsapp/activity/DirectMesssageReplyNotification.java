package screens.com.example.notificationsapp.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.RemoteInput;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import screens.com.example.notificationsapp.MainActivity;
import screens.com.example.notificationsapp.R;

public class DirectMesssageReplyNotification extends AppCompatActivity implements View.OnClickListener {

    String KEY_REPLY = "key_reply";
    public static final int NOTIFICATION_ID = 1;

    Button btnBasicInlineReply;
    TextView txtReplied;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direct_messsage_reply_notification);
        clearExistingNotifications();

        btnBasicInlineReply = (Button) findViewById(R.id.inline_btn);
        txtReplied = (TextView) findViewById(R.id.textView);
        btnBasicInlineReply.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.inline_btn:

                //Create notification builder
                NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                        .setSmallIcon(android.R.drawable.stat_notify_chat)
                        .setContentTitle("Inline Reply Notification");

                String replyLabel = "Enter your reply here";

                //Initialise RemoteInput
                RemoteInput remoteInput = new RemoteInput.Builder(KEY_REPLY)
                        .setLabel(replyLabel)
                        .build();

                //PendingIntent that restarts the current activity instance.
                    Intent resultIntent = new Intent(this, MainActivity.class);
                resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                //Notification Action with RemoteInput instance added.
                NotificationCompat.Action replyAction = new NotificationCompat.Action.Builder
                        (android.R.drawable.sym_action_chat, "REPLY", resultPendingIntent)
                        .addRemoteInput(remoteInput)
                        .setAllowGeneratedReplies(true)
                        .build();

                //Notification.Action instance added to Notification Builder.
                builder.addAction(replyAction);

                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("notificationId", NOTIFICATION_ID);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent dismissIntent = PendingIntent.getActivity(getBaseContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);


                builder.addAction(android.R.drawable.ic_menu_close_clear_cancel, "DISMISS", dismissIntent);

                //Create Notification.
                NotificationManager notificationManager =
                        (NotificationManager)
                                getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(NOTIFICATION_ID,
                        builder.build());
                break;

        }
    }

    private void clearExistingNotifications()
    {
        int notificationId = getIntent().getIntExtra("notificationId", 0);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(notificationId);
    }
}




