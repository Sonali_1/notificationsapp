package screens.com.example.notificationsapp;

import android.app.Activity;
import android.app.Notification;
import android.app.Notification.Style;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.Button;

import screens.com.example.notificationsapp.activity.BatteryLow;
import screens.com.example.notificationsapp.activity.EmailReceived;
import screens.com.example.notificationsapp.activity.StorageFull;

public class MainActivity extends Activity {

    private NotificationManager mNotificationManager;
    private int SIMPLE_NOTIFICATION_ID_BATTERYLOW = 0;
    private int SIMPLE_NOTIFICATION_ID_STORAGEFULL = 1;
    private int SIMPLE_NOTIFICATION_ID_EMAIL = 2;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        button = findViewById(R.id.btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayNotification("Extra for BatteryLow", "Battery is Low ",
                        " Phone Battery is very Low", BatteryLow.class, SIMPLE_NOTIFICATION_ID_BATTERYLOW, R.drawable.ic_battery_alert);

                displayNotification("Extra for StorageFull", "Storage is Full",
                        "Phone Storage is Full", StorageFull.class, SIMPLE_NOTIFICATION_ID_STORAGEFULL, R.drawable.ic_call);

                displayNotification("Extra for Email", "Gmail Notification",
                        "New email received", EmailReceived.class, SIMPLE_NOTIFICATION_ID_EMAIL, R.drawable.ic_alert);
            }
        });
    }

    private void displayNotification(String extra, String contentTitle, String contentText, Class<?> cls, int id, int drawable) {
        final String someLongText = "......";
        Notification notifyDetails = new Notification(R.drawable.ic_notifications, "New Alert!", System.currentTimeMillis());
        Intent intent = new Intent(this, cls);
        intent.putExtra("extra", extra);
        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), id, intent, PendingIntent.FLAG_ONE_SHOT);
        Notification.Builder builder = new Notification.Builder(this)
                   .setSmallIcon(drawable)
                   .setContentTitle(contentTitle)
                   .setContentText(contentText)
                   .setContentIntent(contentIntent);
          notifyDetails = builder.build();
        mNotificationManager.notify(id, notifyDetails);
    }


    public void cancelNotification(View view) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(ns);
        nMgr.cancel(1);
    }

}

