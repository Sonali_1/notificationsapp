package screens.com.example.notificationsapp.activity;

import android.app.Activity;
import android.os.Bundle;

import screens.com.example.notificationsapp.R;

/**
 * Created by sonalisharma on 29/03/18.
 */

public class StorageFull extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storage_full);
    }
}
